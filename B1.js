function tinhThue() {
  var name = document.getElementById("name").value;
  var income = document.getElementById("income").value * 1;
  var soNguoi = document.getElementById("soNguoi").value * 1;
  var thuNhapChiuThue = income - 4000000 - soNguoi * 1600000;
  var thue;
  if (0 < thuNhapChiuThue && thuNhapChiuThue <= 60000000)
    thue = thuNhapChiuThue * 0.05;
  else if (thuNhapChiuThue <= 120000000) thue = thuNhapChiuThue * 0.1;
  else if (thuNhapChiuThue <= 210000000) thue = thuNhapChiuThue * 0.15;
  else if (thuNhapChiuThue <= 384000000) thue = thuNhapChiuThue * 0.2;
  else if (thuNhapChiuThue <= 624000000) thue = thuNhapChiuThue * 0.25;
  else if (thuNhapChiuThue <= 960000000) thue = thuNhapChiuThue * 0.3;
  else if (thuNhapChiuThue > 960000000) thue = thuNhapChiuThue * 0.35;
  document.getElementById("ketQuaB1").innerHTML = `
    <div>Họ tên: ${name}</div>
    <div>Số tiền thuế phải trả: ${thue}vnd</div>
  `;
  document.getElementById("ketQuaB1").style.backgroundColor = "#d1e7dd";
  document.getElementById("ketQuaB1").style.padding = "10px";
  document.getElementById("ketQuaB1").style.marginTop = "10px";
}
