var loaiKhachHang = document.getElementById("userType");
loaiKhachHang.addEventListener("click", function () {
  var selectValue = document.getElementById("userType").value;
  if (selectValue == "1") document.getElementById("soKetNoi").disabled = true;
  else document.getElementById("soKetNoi").disabled = false;
});
function tinhTienCap() {
  var code = document.getElementById("code").value;
  var userType = document.getElementById("userType").value;
  var soKetNoi = document.getElementById("soKetNoi").value;
  var soKenhCaoCap = document.getElementById("soKenhCaoCap").value;
  var soTienCap;
  var soTienKetNoi;
  console.log({ userType });
  if (userType == "1") {
    soTienCap = 4.5 + 20.5 + soKenhCaoCap * 7.5;
  } else {
    if (soKetNoi <= 10) soTienKetNoi = 75;
    else soTienKetNoi = 75 + (soKetNoi - 10) * 5;
    soTienCap = 15 + soTienKetNoi + soKenhCaoCap * 50;
  }
  document.getElementById("ketQuaB2").innerHTML = `
    <div>Mã khách hàng: ${code}</div>
    <div>Số tiền cáp phải trả: $${soTienCap}</div>
  `;
  document.getElementById("ketQuaB2").style.backgroundColor = "#d1e7dd";
  document.getElementById("ketQuaB2").style.padding = "10px";
  document.getElementById("ketQuaB2").style.marginTop = "10px";
}
